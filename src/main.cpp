#include <cassert>
#include <cstdio>
#include <vector>
#include <queue>

constexpr int INF = std::numeric_limits<int>::max();

using node = std::pair<int, int>; //weight, index -> weight first to be key for queue
using graph = std::vector< std::vector<node> >;

int executePrim(graph& graph)
{
	std::priority_queue< node, std::vector<node>, std::greater<> > queue; //to have ElogV complexity
	std::vector<bool>inTree(graph.size(), false); //bitset requires constant size so vector here
	std::vector<int> keys(graph.size(), INF);
	std::vector<int>primTree(graph.size(), 0);

	int start = 0;
	int sum = 0;
	queue.push({ 0, start }); //add first node with 0 weight
	keys[start] = 0;

	while (!queue.empty())
	{
		const int u = queue.top().second; //get the index, no pop yet
		queue.pop();
		if (inTree[u]) //small optimization - vert can occur multiple times already
		{
			continue;
		}
		inTree[u] = true;
		//Search all neighbours --> adjacent verts of 'u'
		for (auto& n: graph[u])
		{
			int vert = n.second;
			int weight = n.first;
			//Not in Tree and visited by vert in tree (min)
			if (!inTree[vert] && keys[vert] > weight)
			{
				keys[vert] = n.first; //updating weight
				queue.push({ keys[vert], vert });
				primTree[vert] = weight;
			}
		}
	}
	for (auto& vert : primTree)
	{
		sum += vert;
	}
	return sum;
}

int main()
{
	FILE* input = nullptr;
	const errno_t errInput = fopen_s(&input, "./tests/input", "r");
	FILE* output = nullptr;
	const errno_t errOutput = fopen_s(&output, "./tests/main_out", "w");
	assert(errInput == 0 && "Failed to open file");
	assert(errOutput == 0 && "Failed to create file");

	int tests = 0, verts = 0, edges = 0;
	//All tests loop
	fscanf_s(input, "%d%*c", &tests);
	for (int t = 0; t < tests; ++t)
	{
		fscanf_s(input, "n=%d,m=%d%*c", &verts, &edges);
		graph graph(verts);

		int fromV, toV, weight;
		for (int i = 0; i < edges; ++i)
		{
			fscanf_s(input, "{%d,%d}%d ", &fromV, &toV, &weight);
			graph[fromV].emplace_back(weight, toV);
			graph[toV].emplace_back(weight, fromV);
		}

		const int solution = executePrim(graph);
		fprintf_s(output, "%d\n", solution);
	}

	fclose(input);
	fclose(output);
	return 0;
}